from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.urls import reverse


class Lesson(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.CharField(max_length=1023, verbose_name='Описание', null=True)
    start_time = models.TimeField(verbose_name='Время начала')
    end_time = models.TimeField(verbose_name='Время окончания')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    preview = models.ImageField(verbose_name='Изображение', null=True)
    category = models.ManyToManyField('Category', verbose_name='Категории', blank=True)
    course = models.ForeignKey('Course', on_delete=models.CASCADE, verbose_name='Курс', null=True)
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Преподаватель', null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('lesson', kwargs={'lesson_id': self.pk})

    class Meta:
        verbose_name = 'Урок'
        verbose_name_plural = 'Уроки'


class Category(models.Model):
    title = models.CharField(max_length=63, verbose_name='Название')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Review(models.Model):
    description = models.CharField(max_length=1023, verbose_name='Содержание')
    author = models.CharField(max_length=255, verbose_name='Автор')
    category = models.CharField(max_length=127, verbose_name='Категория', null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'


class Estimation(models.Model):
    lesson = models.ForeignKey('Lesson', on_delete=models.CASCADE, verbose_name='Урок')
    course = models.ForeignKey('Course', on_delete=models.CASCADE, verbose_name='Курс', null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Ученик', null=True, related_name='user')
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Преподаватель', related_name='teacher')
    score = models.CharField(max_length=10, verbose_name='Оценка')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата выставления')

    def __str__(self):
        return self.score

    class Meta:
        verbose_name = 'Оценка'
        verbose_name_plural = 'Оценки'


class Course(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.CharField(max_length=1023, verbose_name='Описание', null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    preview = models.ImageField(verbose_name='Изображение', null=True)
    category = models.ManyToManyField('Category', verbose_name='Категории', blank=True)
    user = models.ManyToManyField(User, verbose_name='Участники', blank=True)

    def __str__(self):
        return self.title

    def get_count_lessons(self):
        return self.lesson_set.count()

    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'