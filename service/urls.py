from django.urls import path

from .views import *

urlpatterns = [
    path('', index, name="home"),
    path('lessons', lessons, name="lessons"),

    path('lessons/<int:lesson_id>', get_lesson, name="lesson"),
    path('lessons/<int:lesson_id>/delete', delete_lesson),
    path('lessons/<int:lesson_id>/edit', edit_lesson),

    path('lessons/new', create_lesson, name="lesson-create"),

    path('about', about, name="about"),
    path('reviews', reviews, name="reviews"),

    path('estimations', estimations, name="estimations"),

    path('login', UserAuth.as_view(), name="login"),
    path('logout', UserLogout, name="logout"),
    path('registration', UserCreate.as_view(), name="registration"),

    path('account', account, name="account"),
    path('account/edit', account, name="account-edit"),
    path('account/remove', account, name="account-remove"),

    path('courses', courses, name="courses")
]
