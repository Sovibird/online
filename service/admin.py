from django.contrib import admin

from .models import *

# Register your models here.


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'author', 'created_at')
    list_display_links = ('id', 'author')
    search_fields = ('id', 'author')
    list_filter = ('created_at', )


class LessonAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'course', 'teacher')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'category', 'course', 'description')
    list_filter = ('created_at', 'start_time', 'end_time')
    filter_horizontal = ('category', )


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title')


class CourseAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'created_at')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title', 'description', 'category')
    list_filter = ('created_at',)
    filter_horizontal = ('category', 'user')


class EstimationAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'lesson', 'score')
    list_display_links = ('id', 'user', 'lesson')
    search_fields = ('id', 'user', 'lesson', 'teacher')
    list_filter = ('score', 'created_at')


admin.site.register(Review, ReviewAdmin)
admin.site.register(Lesson, LessonAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Estimation, EstimationAdmin)
