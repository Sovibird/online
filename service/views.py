from django.contrib.auth import logout
from django.contrib.auth.views import LoginView
from django.db.models import Count, Sum, Avg
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView

from .forms import RegisterUserForm, AuthorizationUserForm
from .models import *


class UserCreate(CreateView):
    form_class = RegisterUserForm
    template_name = 'registration.html'
    success_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Регистрация нового пользователя'
        return context


class UserAuth(LoginView):
    form_class = AuthorizationUserForm
    template_name = 'authorization.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Авторизация'
        return context

    def get_success_url(self):
        return reverse_lazy('home')


def pageNotFound(request, exception):
    pass


def index(request):
    context = {
        'title': 'Online. Лучшая онлайн-школа России'
    }
    return render(request, 'home.html', context=context)


def lessons(request):
    if not request.user.is_authenticated:
        return redirect('home')

    lessons_ = Lesson.objects.all()
    context = {
        'lessons': lessons_,
        'title': 'Список уроков и курсов'
    }
    return render(request, 'lesson/page.html', context=context)


def get_lesson(request, lesson_id):
    if not request.user.is_authenticated:
        return redirect('home')

    lesson = get_object_or_404(Lesson, pk=lesson_id)
    context = {
        'lesson': lesson,
        'title': lesson.title
    }
    return render(request, 'lesson/single.html', context=context)


def create_lesson(request):
    if not request.user.is_authenticated:
        return redirect('home')
    pass


def delete_lesson(request):
    if not request.user.is_authenticated:
        return redirect('home')
    pass


def edit_lesson(request):
    if not request.user.is_authenticated:
        return redirect('home')
    pass


def about(request):
    pass


def reviews(request):
    reviews_ = Review.objects.all()
    context = {
        'reviews': reviews_,
        'title': 'Отзывы и комментарии'
    }
    return render(request, 'reviews/page.html', context=context)


def UserLogout(request):
    logout(request)
    return redirect('login')


def account(request):
    if not request.user.is_authenticated:
        return redirect('home')

    context = {}

    if request.method == 'POST':
        user = request.user
        user.first_name = request.POST['first_name']
        user.last_name = request.POST['second_name']
        user.save()
        context['notify'] = 'Данные успешно обновлены'

    context['title'] = request.user.first_name + ' ' + request.user.last_name + '. Личный кабинет'

    return render(request, 'account/single.html', context=context)


def estimations(request):
    if not request.user.is_authenticated:
        return redirect('home')

    estimations_ = request.user.course_set.all().annotate(
        lesson_count=Count('lesson', distinct=True),
        estimation_count=Count('estimation', distinct=True),
        estimation_avg=Avg('estimation__score', distinct=True),
        progress=Count('estimation', distinct=True) / Count('lesson', distinct=True) * 100
    ).order_by('-estimation_count')

    context = {
        'estimations': estimations_,
        'title': 'Успеваемость'
    }
    return render(request, 'estimation/page.html', context=context)


def courses(request):
    courses_ = Course.objects.all()
    context = {
        'courses': courses_,
        'title': 'Список курсов'
    }
    return render(request, 'course/page.html', context=context)