from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django import forms


class RegisterUserForm(UserCreationForm):
    username = forms.CharField(label='Имя пользователя', widget=forms.TextInput(attrs={'class': 'form__input', 'placeholder': 'Введите имя пользователя'}))
    first_name = forms.CharField(label='Имя', widget=forms.TextInput(attrs={'class': 'form__input', 'placeholder': 'Введите Ваше имя'}))
    last_name = forms.CharField(label='Фамилия', widget=forms.TextInput(attrs={'class': 'form__input', 'placeholder': 'Введите Вашу фамилию'}))
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={'class': 'form__input', 'placeholder': 'Введите Ваш Email'}))
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form__input', 'placeholder': 'Введите пароль'}))
    password2 = forms.CharField(label='Повторите пароль', widget=forms.PasswordInput(attrs={'class': 'form__input', 'placeholder': 'Введите пароль еще раз'}))

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', 'first_name', 'last_name', 'email')


class AuthorizationUserForm(AuthenticationForm):
    username = forms.CharField(label='Имя пользователя', widget=forms.TextInput(attrs={'class': 'form__input', 'placeholder': 'Введите имя пользователя'}))
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form__input', 'placeholder': 'Введите пароль'}))